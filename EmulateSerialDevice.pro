TEMPLATE = app
TARGET = EmulateSerialDevice
DEPENDPATH += .
INCLUDEPATH += .

QT += widgets serialport


ARCH = "x86"
contains(QMAKE_HOST.arch, x86_64){
    ARCH = "x86_64"
}
CONFIG(debug, release|debug){
    MOC_DIR = .build/$$ARCH/debug
    OBJECTS_DIR = .build/$$ARCH/debug
    UI_DIR = .build/$$ARCH/debug
    RCC_DIR = .build/$$ARCH/debug
}
CONFIG(release, release|debug){
    MOC_DIR = .build/$$ARCH/release
    OBJECTS_DIR = .build/$$ARCH/release
    UI_DIR = .build/$$ARCH/release
    RCC_DIR = .build/$$ARCH/release
}
QMAKE_LINK_OBJECT_SCRIPT = .build/$$ARCH/object_script

win32 {
    RC_FILE = EmulateSerialDevice.rc
}

DESTDIR = Bin

gcc {
    QMAKE_CXXFLAGS += -std=gnu++11
    QMAKE_CXXFLAGS += -pedantic -pedantic-errors
    QMAKE_CXXFLAGS += -Wall -Wextra -Wformat -Wformat-security -Wno-unused-variable -Wno-unused-parameter

    LIBS += -llua

    win32 {
        INCLUDEPATH  += $(CPLUS_INCLUDE_PATH)
        QMAKE_LIBDIR += $(LIBRARY_PATH)
    }
}


# Input
SOURCES += main.cpp \ 
    mainwindow.cpp

FORMS += \
    mainwindow.ui

HEADERS += \
    mainwindow.h \
    lua/LuaContext.hpp
