#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSerialPortInfo>
#include <QDir>
#include <QFile>
#include <QDebug>

#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(this->width(),this->height());
    this->setWindowTitle("Emulate Serial Device");

    currentIndexDevice=-1;
    setSerialStatus("disconnect",1);

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()){
        qDebug() << info.portName() << " " << QString::number(info.productIdentifier()) << " " << QString::number(info.vendorIdentifier()) << " " << info.description() << " " << info.manufacturer();
        ui->comboBox_2->addItem(info.portName());
    }

    QDir dir("devices");
    if (dir.exists()==true){
        dir.setFilter(QDir::AllDirs | QDir::Files | QDir::Hidden | QDir::NoDotAndDotDot);
        QFileInfoList list = dir.entryInfoList();
        for (int i = 0; i < list.size(); ++i) {
            QFileInfo fileInfo = list.at(i);
            if (fileInfo.isFile()==true){
                if (fileInfo.fileName().mid(fileInfo.fileName().length()-4)==".lua"){
                    if (addVirtualDevice(fileInfo.absoluteFilePath())){
                        LuaContext *lua = listDevices.at(listDevices.size()-1);

                        QString version = QString::fromStdString(lua->readVariable<std::string>("version"));
                        QString description = QString::fromStdString(lua->readVariable<std::string>("description"));

                        ui->comboBox->addItem(description+" ("+version+")",listDevices.size()-1);
                    }
                }
            }
        }
    }
    if (ui->comboBox->count()>0){
        ui->comboBox->setCurrentIndex(0);
        setDevice();
    }

    connect(&m_serialPort, &QSerialPort::readyRead, this, &MainWindow::serialReadyRead);
    connect(&m_serialPort, static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error), this, &MainWindow::serialError);

    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(serialConnect()));
    connect(ui->pushButton_2,SIGNAL(clicked(bool)),this,SLOT(serialDisconnect()));
    connect(ui->pushButton_3,SIGNAL(clicked(bool)),this,SLOT(close()));
    connect(ui->pushButton_4,SIGNAL(clicked(bool)),this,SLOT(setDevice()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::addVirtualDevice(QString filename){
    QString result;

    try {
        LuaContext *lua = new LuaContext();

        QFile file(filename);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return false;
        QTextStream in(&file);
        QString data=in.readAll();
        file.close();

        lua->writeFunction("SendToSerial", [this](std::string text) {
                if (m_serialPort.isOpen()){
                    ui->textEdit_2->setText(ui->textEdit_2->toPlainText()+QString::fromStdString(text));
                    m_serialPort.write(text.data(),text.length());
                }
        });

        lua->executeCode(data.toStdString());

        listDevices.push_back(lua);

        return true;

    }
    catch(const LuaContext::ExecutionErrorException& e) {
        //std::cout << e.what() << std::endl;           // prints an error message
        result=e.what();
        try {
            std::rethrow_if_nested(e);
        } catch(const std::runtime_error& e) {
            qDebug() << e.what();
        }
    }
    catch(const LuaContext::WrongTypeException& e) {
        result=e.what();
    }
    catch(const LuaContext::SyntaxErrorException& e) {
        result=e.what();
    }
    catch(const std::runtime_error& e) {
        result=e.what();
    }
    catch(...){
        result="unknow error";
    }

    qDebug() << result;
    return false;
}

void MainWindow::serialConnect(){
    //initSerialPort();
    m_serialPort.setPortName(ui->comboBox_2->currentText());

    switch (ui->comboBox_3->currentText().toInt()) {
        case 1200:
            m_serialPort.setBaudRate(QSerialPort::Baud1200);
            break;
        case 2400:
            m_serialPort.setBaudRate(QSerialPort::Baud2400);
            break;
        case 4800:
            m_serialPort.setBaudRate(QSerialPort::Baud4800);
            break;
        case 9600:
            m_serialPort.setBaudRate(QSerialPort::Baud9600);
            break;
        case 19200:
            m_serialPort.setBaudRate(QSerialPort::Baud19200);
            break;
        case 38400:
            m_serialPort.setBaudRate(QSerialPort::Baud38400);
            break;
        case 57600:
            m_serialPort.setBaudRate(QSerialPort::Baud57600);
            break;
        case 115200:
            m_serialPort.setBaudRate(QSerialPort::Baud115200);
            break;
        default:
            m_serialPort.setBaudRate(QSerialPort::Baud9600);
            break;
    }

    switch (ui->comboBox_4->currentText().toInt()) {
        case 5:
            m_serialPort.setDataBits(QSerialPort::Data5);
            break;
        case 6:
            m_serialPort.setDataBits(QSerialPort::Data6);
            break;
        case 7:
            m_serialPort.setDataBits(QSerialPort::Data7);
            break;
        case 8:
            m_serialPort.setDataBits(QSerialPort::Data8);
            break;
    }

    if (ui->comboBox_5->currentText()=="NoParity"){
        m_serialPort.setParity(QSerialPort::NoParity);
    } else if (ui->comboBox_5->currentText()=="EvenParity"){
        m_serialPort.setParity(QSerialPort::EvenParity);
    } else if (ui->comboBox_5->currentText()=="OddParity"){
        m_serialPort.setParity(QSerialPort::OddParity);
    } else if (ui->comboBox_5->currentText()=="SpaceParity"){
        m_serialPort.setParity(QSerialPort::SpaceParity);
    } else if (ui->comboBox_5->currentText()=="MarkParity"){
        m_serialPort.setParity(QSerialPort::MarkParity);
    }

    switch (ui->comboBox_4->currentIndex()) {
        case 0:
            m_serialPort.setStopBits(QSerialPort::OneStop);
            break;
        case 1:
            m_serialPort.setStopBits(QSerialPort::OneAndHalfStop);
            break;
        case 2:
            m_serialPort.setStopBits(QSerialPort::TwoStop);
            break;
    }

    switch (ui->comboBox_5->currentIndex()) {
        case 0:
            m_serialPort.setFlowControl(QSerialPort::NoFlowControl);
            break;
        case 1:
            m_serialPort.setFlowControl(QSerialPort::HardwareControl);
            break;
        case 2:
            m_serialPort.setFlowControl(QSerialPort::SoftwareControl);
            break;
    }


    if (!m_serialPort.open(QIODevice::ReadWrite)) {
        ui->label_4->setText(m_serialPort.errorString());
    }else{
        setSerialStatus("connect",0);
    }
}

void MainWindow::setSerialStatus(QString text,int code){
    QPalette palette;

    if (code==1){   // Ошибка
        QBrush brush(QColor(255, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        QBrush brush1(QColor(120, 120, 120, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
    }
    if (code==0){  // Готово
        QBrush brush(QColor(0, 150, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        QBrush brush1(QColor(120, 120, 120, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
    }

    ui->label_4->setText(text);
    ui->label_4->setPalette(palette);
}

void MainWindow::serialDisconnect(){
    m_serialPort.close();
    setSerialStatus("disconnect",1);
}

void MainWindow::serialReadyRead(){
    QByteArray data=m_serialPort.readAll();

    ui->textEdit->setText(ui->textEdit->toPlainText()+data);

    try {
        if (currentIndexDevice!=-1){
            LuaContext *lua = listDevices.at(currentIndexDevice);
            const auto function = lua->readVariable<std::function<void (std::string)>>("ReadyRead");

            for (int i=0;i<data.size();i++){
                function(data.mid(i,1).toStdString());
            }
        }
    }catch (...){
        qDebug() << "error serialReadyRead";
    }

}

void MainWindow::serialError(QSerialPort::SerialPortError error){
    ui->label_4->setText(m_serialPort.errorString());
    qDebug() << error << " " <<  m_serialPort.errorString();
}

void MainWindow::setDevice(){
    currentIndexDevice=ui->comboBox->currentData().toInt();

    try {
        LuaContext *lua = listDevices.at(currentIndexDevice);

        const auto function = lua->readVariable<std::function<void (void)>>("init");
        function();

        QString description = QString::fromStdString(lua->readVariable<std::string>("description"));
        ui->label_11->setText(description);
    }catch (...){
        qDebug() << "error setDevice";
    }
}

