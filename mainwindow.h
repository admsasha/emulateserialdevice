#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>

#include "lua/LuaContext.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private:
        bool addVirtualDevice(QString filename);
        void setSerialStatus(QString text, int code);

        Ui::MainWindow *ui;

        QSerialPort m_serialPort;

        QVector<LuaContext*> listDevices;
        int currentIndexDevice;


    private slots:
        void serialConnect();
        void serialDisconnect();
        void serialReadyRead();
        void serialError(QSerialPort::SerialPortError error);

        void setDevice();
};

#endif // MAINWINDOW_H
