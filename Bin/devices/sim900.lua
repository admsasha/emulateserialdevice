﻿version = "1.0.0"
description = "module SIM900"

local buffer = ""
local static_answer = {}

-- статус модема WAIT_Z | WAIT_R
local status_modem = ""

function init()
	status_modem=""

	static_answer = {
		["AT+GMM"]="SIMCOM_SIM900\n",
		["AT+GOI"]="SIM900\n",
		["AT+GMR"]="Revision:1137B09SIM900M64_ST\n",
		["AT+GSN"]="013226001234567\n",

		["AT+COPS?"]="+COPS: 0,0,\"MTS-RUS\"\n",
		["AT+COPS=?"]="+COPS: (2,\"MTS RUS\",\"\",\"25001\"),(1,\"MOTIV\",\"MOTIV\",\"25035\"),(1,\"Utel\",\"Utel\",\"25039\"),,(0,1,4),(0,1,2)\n",
		["AT+CPAS"]="+CPAS: 0\n",
		["AT+AT+CREG?"]="+CREG: 0,1\n",
		["AT+CSQ"]="+CSQ: 17,0\n",
		["AT+CCLK?"]="+CCLK: \"00/01/01,14:31:27+00\"\n",
		["AT+CBC"]="+CBC: 0,95,4134\n",
		["AT+CADC?"]="+CADC: 1,7\n",
		
		["AT+IPR?"]="+IPR: 0\n",
		["AT+ICF?"]="+ICF: 3,3\n",
		["AT+IFC?"]="+IFC: 0,0",

		["ATDL"]="",
		["ATA"]="",
		["ATH0"]="",

		["AT+CSCA?"]="+CSCA: \"+79126313431\",145",

	}
end

function ReadyRead(data)	
	SendToSerial(data)

	if status_modem=="WAIT_Z" then
		if string.byte(data)==0x1A or string.byte(data)==0x1B then
			status_modem="WAIT_R"
		end
		return
	end
	if status_modem=="WAIT_R" then
		if string.byte(data)==0x0D or string.byte(data)==0x0A then
			buffer=""
			status_modem =""
			SendToSerial("+CMGS: 115\n")
			SendToSerial("OK\r")
		end
		return;
	end



	if string.byte(data)==0x0D or string.byte(data)==0x0A then
		if static_answer[buffer] ~= nil then
			SendToSerial(static_answer[buffer])
			SendToSerial("OK\r")
		else	
			if buffer=="AT" then
				SendToSerial("OK\r")

			elseif string.match(buffer,"^(AT%+CMGF=%d)$") ~= nil then
				sp,n = string.match(buffer,"(AT%+CMGF=)(%d)")
				if tonumber(n)==0 or tonumber(n)==1 then
					SendToSerial("OK\r")
				else
					SendToSerial("ERROR\r")
				end

			elseif string.match(buffer,"^(AT%+CSCS=\")(.+)(\")$") ~= nil then

				SendToSerial("OK\r")

			elseif string.match(buffer,"^(AT%+CSCB=[0|1])$") ~= nil then
				SendToSerial("OK\r")

			elseif string.match(buffer,"^(AT%+CLIP=[0|1])$") ~= nil then
				SendToSerial("OK\r")

			elseif string.match(buffer,"^(AT%+GSMBUSY=[0|1])$") ~= nil then
				SendToSerial("OK\r")

			elseif string.match(buffer,"^(ATS0=[0|1])$") ~= nil then
				SendToSerial("OK\r")

			elseif string.match(buffer,"^(AT%+CMGS=.+)$") ~= nil then
				status_modem="WAIT_Z"
				SendToSerial(">")

			elseif string.match(buffer,"^(AT%+CMGDA=\"%a+\")$") ~= nil then
				SendToSerial("OK\r")

			elseif string.match(buffer,"^(AT%+CMGD=%d+)$") ~= nil then
				SendToSerial("OK\r")

			else
				SendToSerial("ERROR\r")
			end
		end
		buffer=""
	else
		buffer=buffer..data
	end

end