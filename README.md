# README #

Emulation devices running on serial port.
The EmulateSerialDevice serves to test your programs interacting with devices working on serial ports.

[EmulateSerialDevice] <-virtual com port-> [your program]

VIRTUAL SERIAL PORTS EMULATOR: http://www.eterlogic.com/downloads/SetupVSPE.zip


![EmulateSerialDevice images](images/EmulateSerialDevice.png)